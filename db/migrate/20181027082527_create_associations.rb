class CreateAssociations < ActiveRecord::Migration
  def change
    create_table :associations do |t|
      t.integer :note_id
      t.integer :topic_id

      t.timestamps null: false
    end
  end
end
