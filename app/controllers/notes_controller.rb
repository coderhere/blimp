class NotesController < ApplicationController
  def index
  	@topics=Topic.all.order('created_at::date DESC').order('freq DESC')
  	if request.url.include?('notes')
  		topic_id=request.url.split("/")[-1]
  		notes_ids=Association.where(:topic_id=>topic_id).pluck(:note_id)
  		@notes=Note.where(:id=>notes_ids)
  	else
  		@notes=Note.all.order('created_at DESC')
  	end
  	
  end
  def create
	notes=params[:note][:note]
	words=notes.split(" ")
	topics=words.map{|e| e.include?("#") ? e:nil }
	final_topics=topics.compact
	topic_ids=[]
	final_topics.each do |topic|
		topic_from_db=Topic.where(:name=>topic).first
		if topic_from_db.present?
			f=topic_from_db.freq
			topic_from_db.freq=f+1;
			topic_from_db.save!
			topic_ids.push(topic_from_db.id)
		else
			new_topic=Topic.new
			new_topic.name=topic
			new_topic.freq=1
			new_topic.save!
			topic_ids.push(new_topic.id)

		end
	end
	note=Note.new
	note.note=notes
	note.save!
	new_note_id=note.id
	topic_ids.each do |id|
		association=Association.new
		association.note_id=new_note_id
		association.topic_id=id
		association.save!
	end
	redirect_to root_path

  end
  def destroy
  	note_id=params[:id]
  	note=Note.find(note_id)
  	note.destroy!
  	associations=Association.where(:note_id=>note_id)
  	topic_ids=associations.pluck(:topic_id)
  	associations.destroy_all
  	topic_ids.each do |id|
  		association=Association.where(:topic_id=>id)
  		if !association.present?
  			Topic.find(id).destroy!
  		end
  	end
  	redirect_to root_path
  end
end
