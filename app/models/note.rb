class Note < ActiveRecord::Base
  has_many :associations
  has_many :topics, through: :associations
end
