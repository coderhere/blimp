class Topic < ActiveRecord::Base
  has_many :associations
  has_many :notes, through: :associations
  validates_uniqueness_of :name
end
